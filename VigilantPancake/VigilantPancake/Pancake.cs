using System;
using System.Collections.Generic;

namespace VigilantPancake
{
    public class Pancake
    {
        public double Radius { get; set; }
        public List<Ingredient> Ingredients { get; }

        public Pancake()
        {
            Ingredients = new List<Ingredient>();
        }

        public void AddIngredient(Ingredient ingredient)
        {
            if (Ingredients.Count >= 5)
            {
                throw new PancakeTooHeavyException();
            }
            Ingredients.Add(ingredient);
        }
    }

    public class PancakeTooHeavyException : Exception
    {
    }
}