using System;

namespace VigilantPancake
{
    public class Ingredient
    {
        public Guid Id { get; }    
        public string Name { get; }    
        
        public Ingredient(string name)
        {
            Id = Guid.NewGuid();
            Name = name;
        }
    }
}